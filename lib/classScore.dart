import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editClass.dart';
import 'package:test_app_flutter/exerciseSets.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/testScore.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'assignments.dart';
import 'chart.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class ClassScore extends StatefulWidget {
  final int classId;
  final ClassScoreTab initialPage;

  ClassScore(
      {@required this.classId, this.initialPage = ClassScoreTab.GeneralScore});

  @override
  _ClassScoreState createState() => _ClassScoreState();
}

class _ClassScoreState extends State<ClassScore> with WidgetsBindingObserver {
  int _initialIndex = 0;
  List _topicScores = [];
  double _classScore;
  List _studentScores;
  List _joinTests;

  bool _scoreLoaded = false;
  bool _joinTestsLoaded = false;
  bool _editClassLoading = false;

  ScrollController _scrollController = ScrollController();

  bool _assignmentsLoaded = false;
  List _assignments = [];

  @override
  void initState() {
    switch (widget.initialPage) {
      case ClassScoreTab.GeneralScore:
        _initialIndex = 0;
        break;
      case ClassScoreTab.Topics:
        _initialIndex = 1;
        break;
      case ClassScoreTab.JoinTests:
        _initialIndex = 2;
        break;
      case ClassScoreTab.Assignments:
        _initialIndex = 3;
        break;
    }
    globals.api
        .call('classScore',
            options: {'class': widget.classId.toString()}, context: context)
        .then((data) {
      setState(() {
        _scoreLoaded = true;
        _classScore = data['response']['score'].toDouble();
        _topicScores = data['response']['topics'];
        _studentScores = data['response']['students'];
      });
    });
    globals.api
        .call('classJoinTests',
            options: {'class': widget.classId.toString()}, context: context)
        .then((data) {
      if (data['response'] is List)
        _joinTests = data['response'].reversed.toList();
      setState(() {
        _joinTestsLoaded = true;
      });
    });
    globals.api
        .call('classAssignments',
            options: {'class': widget.classId.toString()}, context: context)
        .then((data) {
      if (data['response'] is List)
        _assignments = data['response'].reversed.toList();
      setState(() {
        _assignmentsLoaded = true;
      });
    });
    super.initState();
  }

  @override
  void didChangePlatformBrightness() {
    print(WidgetsBinding.instance.window
        .platformBrightness); // should print Brightness.light / Brightness.dark when you switch
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        initialIndex: _initialIndex,
        length: 4,
        child: ResponsiveDrawerScaffold(
          helpPage: HelpPage(
              path: HelpPagePath.classScore,
              label: S.of(context).helpWithClassScore),
          appBarBottom: TabBar(tabs: [
            Tab(
              text: S.of(context).overview,
            ),
            Tab(text: S.of(context).topics),
            Tab(
              text: S.of(context).joinTestsReally,
            ),
            Tab(
              text: S.of(context).assignments,
            )
          ]),
          body: TabBarView(children: [
            TestAppScrollBar(
              controller: _scrollController,
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Column(
                  //shrinkWrap: true,
                  children: <Widget>[
                    TestAppCard(
                      children: <Widget>[
                        (_scoreLoaded)
                            ? ScoreChart(_classScore * 100)
                            : CenterProgress(),
                        ListTile(
                          leading: FaIcon(FontAwesomeIcons.infoCircle),
                          title: Text(S
                              .of(context)
                              .pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents),
                          trailing: OutlineButton(
                            onPressed: _editClass,
                            child: (_editClassLoading)
                                ? CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Theme.of(context).accentColor),
                                  )
                                : Text(S.of(context).editClass),
                          ),
                        )
                      ],
                    ),
                    Container(
                      color: Theme.of(context).cardColor,
                      child: (_scoreLoaded)
                          ? ListView.separated(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                var studentData = _studentScores[index];
                                return ListTile(
                                  title: Text(studentData['info']['name']),
                                  trailing: Text(
                                      (!(studentData['score'] is bool))
                                          ? (studentData['score'] * 100)
                                                  .round()
                                                  .toString() +
                                              '%'
                                          : S.of(context).notAvailable),
                                );
                              },
                              separatorBuilder: (context, index) => Divider(),
                              itemCount: _studentScores.length)
                          : CenterProgress(),
                    )
                  ],
                ),
              ),
            ),
            Container(
              color:
                  (MediaQuery.of(context).platformBrightness == Brightness.dark)
                      ? Theme.of(context).backgroundColor
                      : Theme.of(context).cardColor,
              child: TestAppScrollBar(
                controller: _scrollController,
                child: ListView.separated(
                    controller: _scrollController,
                    itemBuilder: (context, index) {
                      if (index == 0)
                        return ListTile(
                          leading: FaIcon(FontAwesomeIcons.infoCircle),
                          title: Text(S
                              .of(context)
                              .pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents),
                          trailing: OutlineButton(
                            onPressed: _editClass,
                            child: (_editClassLoading)
                                ? CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Theme.of(context).accentColor),
                                  )
                                : Text(S.of(context).editClass),
                          ),
                        );
                      var topicData = _topicScores[index - 1]['topic'];
                      var topicScore = _topicScores[index - 1]['score'];
                      var studentScore = _topicScores[index - 1]['students'];
                      return (AwesomeExpansionTile(
                        leading: Chip(label: Text(topicData['subject'])),
                        title: Text(topicData['name']),
                        children: <Widget>[
                          TestAppCard(children: [
                            ListView.separated(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  if (index == 0)
                                    return (ListTile(
                                      leading:
                                          FaIcon(FontAwesomeIcons.chartPie),
                                      title: Text(topicData['name']),
                                      isThreeLine: true,
                                      subtitle: (topicScore is double ||
                                              topicScore is int)
                                          ? Container(
                                              height: 120,
                                              width: 120,
                                              child: ScoreChart(
                                                (topicScore.toDouble() * 100),
                                                labelText: S.of(context).topic,
                                                height: 84,
                                              ),
                                            )
                                          : Text(S.of(context).noDataAvailable),
                                    ));
                                  var studentData = studentScore[index - 1];
                                  return (ListTile(
                                    leading: FaIcon(FontAwesomeIcons.solidUser),
                                    title: Text(studentData['info']['name']),
                                    trailing: Text(
                                        (studentData['score'] is double ||
                                                studentData['score'] is int)
                                            ? (studentData['score'] * 100)
                                                    .round()
                                                    .toString() +
                                                '%'
                                            : S.of(context).noDataAvailable),
                                  ));
                                },
                                separatorBuilder: (context, index) => Divider(),
                                itemCount: studentScore.length + 1),
                          ])
                        ],
                      ));
                    },
                    separatorBuilder: (context, index) => Divider(),
                    itemCount: _topicScores.length + 1),
              ),
            ),
            (_joinTestsLoaded)
                ? (_joinTests.length != 0)
                    ? Container(
                        color: (MediaQuery.of(context).platformBrightness ==
                                Brightness.dark)
                            ? Theme.of(context).backgroundColor
                            : Theme.of(context).cardColor,
                        child: TestAppScrollBar(
                          controller: _scrollController,
                          child: ListView.builder(
                            controller: _scrollController,
                            itemBuilder: (context, index) => JoinTestScoreTile(
                                id: int.parse(_joinTests[index]['id']),
                                name: _joinTests[index]['joinTest']['name']
                                    .toString(),
                                timestamp:
                                    _joinTests[index]['date'].toString()),
                            itemCount: _joinTests.length,
                          ),
                        ),
                      )
                    : ListTile(
                        leading: FaIcon(FontAwesomeIcons.infoCircle),
                        title: Text(S
                            .of(context)
                            .ooopsYouDidntWriteAnyJoinTestsInThisClass),
                      )
                : CenterProgress(),
            (_assignmentsLoaded)
                ? (_assignments.length != 0)
                    ? Container(
                        color: Theme.of(context).cardColor,
                        child: TestAppScrollBar(
                          controller: _scrollController,
                          child: ListView.separated(
                            separatorBuilder: (c, i) => Divider(),
                            controller: _scrollController,
                            itemBuilder: (context, index) {
                              Map assignmentDetails = _assignments[index];
                              return AssignmentDetailTile(
                                  assignmentDetails: assignmentDetails);
                            },
                            itemCount: _assignments.length,
                          ),
                        ),
                      )
                    : ListTile(
                        leading: FaIcon(FontAwesomeIcons.infoCircle),
                        title: Text(S
                            .of(context)
                            .ooopsYouDidntCreateAnyAssignmentForThisClassYet),
                        trailing: OutlineButton(
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (c) => ExerciseSets())),
                          child: Text(S.of(context).selectExerciseSet),
                        ),
                      )
                : CenterProgress()
          ]),
        ));
  }

  Future<void> _editClass() async {
    setState(() {
      _editClassLoading = true;
    });
    globals.api.call('listClasses', context: context).then((data) {
      if (data['response'] is List) {
        var classData = data['response'].singleWhere((elem) {
          return elem['id'].toString() == widget.classId.toString();
        });
        Navigator.of(context).pop();
        Navigator.of(context).push(MaterialPageRoute(
            builder: (b) =>
                EditClass(id: widget.classId, classData: classData)));

        setState(() {
          _editClassLoading = false;
        });
      }
    });
  }
}

class AssignmentDetailTile extends StatelessWidget {
  final Map assignmentDetails;

  const AssignmentDetailTile({Key key, this.assignmentDetails})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime expiry = DateTime.fromMillisecondsSinceEpoch(
        int.parse(assignmentDetails['expiry'].toString() + '000'));
    return ListTile(
      leading: Text(assignmentDetails['id'].toString()),
      title: Text(assignmentDetails['name'].toString()),
      trailing: (expiry.isBefore(DateTime.now()))
          ? Text(timeago.format(expiry))
          : Text(formatFutureDate(expiry)),
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (c) => AssignmentDetails(
                assignmentDetails: assignmentDetails,
              ))),
    );
  }
}

class AssignmentDetails extends StatefulWidget {
  final Map assignmentDetails;

  const AssignmentDetails({Key key, @required this.assignmentDetails})
      : super(key: key);

  @override
  _AssignmentDetailsState createState() => _AssignmentDetailsState();
}

class _AssignmentDetailsState extends State<AssignmentDetails> {
  ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.classScore,
          label: S.of(context).helpWithAssignments),
      title: S.of(context).assignmentDetails,
      appBarLeading: IconButton(
          icon: FaIcon(FontAwesomeIcons.times),
          onPressed: () => Navigator.of(context).pop()),
      body: TestAppScrollBar(
          child: SingleChildScrollView(
              controller: _controller,
              child: Column(
                children: [
                  TestAppCard(children: [
                    ListTile(
                        title: Text(
                      widget.assignmentDetails['name'],
                      style: Theme.of(context).textTheme.headline6,
                    )),
                    ListTile(
                        title: Text(
                      widget.assignmentDetails['describtion'],
                    )),
                    (widget.assignmentDetails == null ||
                            widget.assignmentDetails['tests'].length == 0)
                        ? ListTile(
                            leading: FaIcon(FontAwesomeIcons.infoCircle),
                            title: Text(
                              S.of(context).nooneSubmittedAnyResultYet,
                            ))
                        : ScoreChart(
                            widget.assignmentDetails['score'].toDouble() * 100,
                            labelText: S.of(context).average,
                          ),
                  ]),
                  if ((widget.assignmentDetails != null &&
                      widget.assignmentDetails['tests'].length != 0))
                    TestAppCard(children: [
                      ListTile(
                          title: Text(
                        S.of(context).submittions,
                        style: Theme.of(context).textTheme.headline6,
                      )),
                      ListView.separated(
                          separatorBuilder: (c, i) => Divider(),
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (c, i) {
                            Map test = widget.assignmentDetails['tests'][i];
                            return AwesomeExpansionTile(
                                title: Text(test['user']['name']),
                                leading: Text(
                                    (double.parse(test['score'].toString()) *
                                                100)
                                            .round()
                                            .toString() +
                                        ' %'),
                                children: [
                                  Padding(
                                      padding: EdgeInsets.only(left: 32),
                                      child: Column(children: [
                                        if (test['late'])
                                          ListTile(
                                              leading: FaIcon(
                                                  FontAwesomeIcons.infoCircle),
                                              title: Text(S
                                                  .of(context)
                                                  .thisSubmissionWasLate),
                                              subtitle: Text(S
                                                      .of(context)
                                                      .submitted +
                                                  timeago.format(DateTime
                                                      .fromMillisecondsSinceEpoch(
                                                          int.parse(test[
                                                                      'timestamp']
                                                                  .toString() +
                                                              '000'))))),
                                        ListTile(
                                          title: Text((double.parse(
                                                          test['completedScore']
                                                              .toString()) *
                                                      100)
                                                  .round()
                                                  .toString() +
                                              S.of(context).correctAnswers),
                                        ),
                                        ListTile(
                                          title: Text((double.parse(
                                                          test['completed']
                                                              .toString()) *
                                                      100)
                                                  .round()
                                                  .toString() +
                                              S.of(context).completed),
                                        ),
                                        ListTile(
                                          title: Text(S.of(context).inTotal +
                                              (double.parse(test['score']
                                                          .toString()) *
                                                      100)
                                                  .round()
                                                  .toString() +
                                              S
                                                  .of(context)
                                                  .areCompletedAndCorrect),
                                        ),
                                        ListTile(
                                          title: OutlineButton(
                                            onPressed: () => Navigator.of(
                                                    context)
                                                .push(MaterialPageRoute(
                                                    builder: (c) => TestScore(
                                                        testId: int.parse(
                                                            test['id']
                                                                .toString()),
                                                        hideEasterEgg: true))),
                                            child: Text(S
                                                .of(context)
                                                .viewDetailedScore),
                                          ),
                                        )
                                      ]))
                                ]);
                          },
                          itemCount: widget.assignmentDetails['tests'].length),
                    ]),
                  TestAppCard(children: [
                    ListTile(
                      title: Text(
                        S.of(context).exercises,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (c, i) {
                          Map exercise =
                              widget.assignmentDetails['exercises'][i];
                          return ListTile(
                              title: Text(exercise['name']),
                              subtitle: Text(exercise['content']));
                        },
                        itemCount: widget.assignmentDetails['exercises'].length)
                  ]),
                ],
              )),
          controller: _controller),
    );
  }
}

enum ClassScoreTab { GeneralScore, Topics, Assignments, JoinTests }

class JoinTestScoreTile extends StatefulWidget {
  final int id;
  final String timestamp;
  final String name;

  JoinTestScoreTile(
      {@required this.id, @required this.name, @required this.timestamp});

  @override
  _JoinTestScoreTileState createState() => _JoinTestScoreTileState();
}

class _JoinTestScoreTileState extends State<JoinTestScoreTile> {
  bool _testLoaded = false;
  Map _testData = Map();
  DateTime _timestamp;
  bool _timestampError = false;
  String _name;

  @override
  void initState() {
    super.initState();
    _name =
        widget.name == 'null' ? S.of(context).originalTestDeleted : widget.name;
    try {
      _timestamp = new DateTime.fromMillisecondsSinceEpoch(
          int.parse(widget.timestamp + '000'));
    } catch (e) {
      _timestampError = true;
    }
  }

  void loadScore(BuildContext context) {
    globals.api
        .call('joinScore',
            options: {'join': widget.id.toString()}, context: context)
        .then((data) {
      setState(() {
        if (data['response'] is Map) _testData = data['response'];
        _testLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) => AwesomeExpansionTile(
          leading: (!_timestampError)
              ? Chip(
                  label: Text(timeago.format(_timestamp)),
                )
              : Tooltip(
                  message: S.of(context).errorReadingDate,
                  child: FaIcon(
                    FontAwesomeIcons.exclamationCircle,
                    color: Colors.red,
                  ),
                ),
          title: Text(_name),
          onExpansionChanged: (opened) {
            if (opened) loadScore(context);
          },
          children: [
            (TestAppCard(children: [
              (_testLoaded)
                  ? (_testData.length != 0)
                      ? ListView.separated(
                          itemBuilder: (context, index) {
                            if (index == 0)
                              return (ListTile(
                                leading: FaIcon(FontAwesomeIcons.chartPie),
                                title: Text(_name),
                                isThreeLine: true,
                                subtitle: (_testData['score'] is double ||
                                        _testData['score'] is int)
                                    ? Container(
                                        height: 120,
                                        width: 120,
                                        child: ScoreChart(
                                          (_testData['score'].toDouble() * 100),
                                          labelText: S.of(context).topic,
                                          height: 84,
                                        ),
                                      )
                                    : Text(S.of(context).noDataAvailable),
                              ));
                            Map userData = _testData['tests'][index - 1];
                            double score;
                            bool scoreAvailable = true;
                            try {
                              score =
                                  double.parse(userData['score'].toString());
                            } catch (e) {
                              scoreAvailable = false;
                            }
                            return Tooltip(
                              message: S.of(context).pressForMoreDetails,
                              child: (ListTile(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (c) => TestScore(
                                            testId: int.parse(userData['id']),
                                            hideEasterEgg: true,
                                          )));
                                },
                                leading: FaIcon(FontAwesomeIcons.solidUser),
                                title: Text(userData['user']['name']),
                                subtitle: (scoreAvailable)
                                    ? null
                                    : Text(S.of(context).noDataAvailable),
                                trailing: (scoreAvailable)
                                    ? Text(
                                        (score * 100).round().toString() + '%')
                                    : null,
                              )),
                            );
                          },
                          separatorBuilder: (context, index) => Divider(),
                          itemCount: _testData['tests'].length + 1,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                        )
                      : ListTile(
                          leading: FaIcon(
                            FontAwesomeIcons.infoCircle,
                            color: Colors.orange,
                          ),
                          title: Text(S.of(context).noDataAvailable),
                        )
                  : CenterProgress(),
            ]))
          ]),
    );
  }
}
