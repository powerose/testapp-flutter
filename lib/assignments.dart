import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/testScore.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class Assignments extends StatefulWidget {
  @override
  _AssignmentsState createState() => _AssignmentsState();
}

class _AssignmentsState extends State<Assignments> {
  bool _loadedAssignments = false;
  List _assignments;
  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.assignments,
          label: S.of(context).helpWithAssignments),
      body: TestAppScrollBar(
        controller: _scrollController,
        child: SingleChildScrollView(
          controller: _scrollController,
          child: TestAppCard(
            children: <Widget>[
              Text(
                S.of(context).assignments,
                style: Theme.of(context).textTheme.headline6,
              ),
              (_loadedAssignments)
                  ? (_assignments.isNotEmpty)
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (c, index) {
                            bool expired = int.parse(
                                    _assignments[index]['expiry'].toString() +
                                        '000') <
                                DateTime.now().millisecondsSinceEpoch;
                            bool notSubmitted =
                                _assignments[index]['completed'] == 0;
                            bool lateSubmit = expired && notSubmitted;
                            DateTime _assignmentExpiry =
                                DateTime.fromMillisecondsSinceEpoch(int.parse(
                                    _assignments[index]['expiry'] + '000'));
                            String _expiryString =
                                formatFutureDate(_assignmentExpiry);
                            return (AwesomeExpansionTile(
                              title: Row(
                                children: [
                                  Text(
                                    _assignments[index]['class'] + ': ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .copyWith(fontWeight: FontWeight.bold),
                                  ),
                                  Text(_assignments[index]['name']),
                                ],
                              ),
                              leading: (_assignments[index]['completed'] == 1)
                                  ? FaIcon(
                                      FontAwesomeIcons.checkCircle,
                                      color: Colors.green,
                                    )
                                  : ((_assignments[index]['completed'] == 0))
                                      ? FaIcon(
                                          FontAwesomeIcons.timesCircle,
                                          color: Colors.deepOrange,
                                        )
                                      : FaIcon(
                                          FontAwesomeIcons.minusCircle,
                                        ),
                              children: [
                                ListView(
                                  //padding: EdgeInsets.only(left: 48),
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  children: [
                                    Divider(),
                                    ListTile(
                                        leading:
                                            FaIcon(FontAwesomeIcons.question),
                                        title: Text(_assignments[index]
                                            ['describtion'])),
                                    ListTile(
                                        leading: FaIcon(
                                            FontAwesomeIcons.calendarCheck),
                                        title: Text(_expiryString)),
                                    ListTile(
                                      leading:
                                          FaIcon(FontAwesomeIcons.chartPie),
                                      title: Text((_assignments[index]
                                                      ['completed'] *
                                                  100)
                                              .round()
                                              .toString() +
                                          S.of(context).completed),
                                    )
                                  ],
                                ),
                                if (lateSubmit)
                                  ListTile(
                                    isThreeLine: true,
                                    leading: FaIcon(
                                      FontAwesomeIcons.infoCircle,
                                      color: Colors.orange,
                                    ),
                                    title: Text(S
                                        .of(context)
                                        .thisAssignmentIsExpriedButYouDidntSubmitAnythingYet),
                                    subtitle: Text(S
                                        .of(context)
                                        .youCanStillSubmitTheAssignmentButYouWontBe),
                                  ),
                                Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    if (!expired || lateSubmit)
                                      Builder(
                                        builder: (context) =>
                                            OutlineButton.icon(
                                                onPressed: () =>
                                                    _workOnAssignment(
                                                        _assignments[index]),
                                                icon: FaIcon(
                                                    FontAwesomeIcons.penNib),
                                                label: Text(S
                                                    .of(context)
                                                    .workOnAssignment)),
                                      ),
                                    if (expired && !notSubmitted)
                                      OutlineButton.icon(
                                          onPressed: () => Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (c) => TestScore(
                                                      testId: int.parse(
                                                          _assignments[index]
                                                                  ['testId']
                                                              .toString())))),
                                          icon: FaIcon(
                                              FontAwesomeIcons.chartLine),
                                          label: Text(S.of(context).viewScore))
                                  ],
                                )
                              ],
                            ));
                          },
                          separatorBuilder: (c, index) => Divider(),
                          itemCount: _assignments.length)
                      : ListTile(
                          leading: FaIcon(FontAwesomeIcons.infoCircle),
                          title: Text(S
                              .of(context)
                              .huaryThereAreCurrentlyNoAssignmentsForYou),
                        )
                  : CenterProgress()
            ],
          ),
        ),
      ),
    ));
  }

  void _workOnAssignment(Map assignment) async {
    setState(() {
      _loadedAssignments = false;
    });
    var response = await globals.api.call('fetchExercises',
        options: {
          'images': true,
          'exercise': jsonDecode(assignment['exercises'])
        },
        context: context);
    List exercises = response['response'];
    setState(() {
      _loadedAssignments = true;
    });
    Navigator.of(context).push(MaterialPageRoute(
        builder: (c) => WriteTest(
              exercises: exercises,
              assignment: assignment,
            )));
  }

  @override
  void initState() {
    globals.api.call('userAssignments', context: context).then((data) {
      if (data['response'] is List) {
        _assignments = data['response'].reversed.toList();
      } else
        _assignments = [];
      setState(() {
        _loadedAssignments = true;
      });
    });
    super.initState();
  }
}

String formatFutureDate(DateTime assignmentExpiry) {
  return "${twoDigits(assignmentExpiry.day)}.${twoDigits(assignmentExpiry.month)}, ${twoDigits(assignmentExpiry.hour)}:${twoDigits(assignmentExpiry.minute)}";
}
