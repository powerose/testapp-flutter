import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/style.dart';

import 'api.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var api = globals.api;
  TextEditingController _passwordResetController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  //bool _notificationsSubscribed = false;
  //Time _notificationTime = Time(17, 0);

  /*@override
  void initState() {
    TestAppPush.isSubscribed().then((value) => setState(() {
          _notificationsSubscribed = value;
        }));
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
        helpPage: HelpPage(
            path: HelpPagePath.settings, label: S.of(context).helpWithSettings),
        body: TestAppScrollBar(
          controller: _scrollController,
          child: ListView(controller: _scrollController, children: [
            TestAppCard(children: [
              Text(
                S.of(context).account,
                style: Theme.of(context).textTheme.headline6,
              ),
              ListTile(
                title: Text(S.of(context).logOut),
                onTap: _logOut,
              ),
              ListTile(
                title: Text(
                  S.of(context).updatePassword + '...',
                ),
                onTap: _updatePassword,
              ),
            ]),
            TestAppCard(children: [
              Text(
                S.of(context).app,
                style: Theme.of(context).textTheme.headline6,
              ),
              ListTile(
                leading: FaIcon(FontAwesomeIcons.globe),
                title: Text(S.of(context).language),
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => SimpleDialog(
                            title: Text('Language'),
                            children: <Widget>[
                              SimpleDialogOption(
                                onPressed: () {
                                  setState(() {
                                    S.load(Locale('en'));
                                    Preferences().save('language', 'en');
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: ListTile(
                                  title: Text('English'),
                                  leading: Image.asset(
                                      'assets/languages/europe.png'),
                                ),
                              ),
                              SimpleDialogOption(
                                onPressed: () {
                                  setState(() {
                                    S.load(Locale('de'));
                                    Preferences().save('language', 'de');
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: ListTile(
                                  title: Text('Deutsch'),
                                  leading: Image.asset(
                                      'assets/languages/germany.png'),
                                ),
                              ),
                              SimpleDialogOption(
                                onPressed: () {
                                  setState(() {
                                    S.load(Locale('fr'));
                                    Preferences().save('language', 'fr');
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: ListTile(
                                  title: Text('Français'),
                                  leading: Image.asset(
                                      'assets/languages/france.png'),
                                ),
                              ),
                              SimpleDialogOption(
                                onPressed: () {
                                  setState(() {
                                    S.load(Locale('tlh'));
                                    Preferences().save('language', 'tlh');
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: ListTile(
                                  title: Text('tlhIngan Hol'),
                                  leading: FloatingActionButton(
                                    heroTag: 'tlh',
                                    backgroundColor: Colors.red,
                                    elevation: 0,
                                    onPressed: () {},
                                    child: Image.asset(
                                        'assets/languages/klingon.png'),
                                  ),
                                ),
                              ),
                            ],
                          ));
                },
              ), /*
              TestAppCard(
                children: <Widget>[
                  Text('App settings'),
                  SwitchListTile(
                    title: Text('Use dark mode'),
                    value: (Provider.of<ThemeNotifier>(context).brightness ==
                        Brightness.dark),
                    onChanged: (newValue) {
                      Brightness newBrightness =
                          (newValue) ? Brightness.dark : Brightness.light;
                      Provider.of<ThemeNotifier>(context).brightness =
                          newBrightness;
                    },
                  ),
                  SwitchListTile(
                    title: Text('Use pink theme'),
                    value: (Provider.of<ThemeNotifier>(context).primaryColor ==
                        Colors.pink),
                    onChanged: (newValue) {
                      MaterialColor newColor = (newValue)
                          ? Colors.pink
                          : Provider.of<ThemeNotifier>(context).primaryColor;
                      Provider.of<ThemeNotifier>(context).primaryColor =
                          newColor;
                    },
                  )
                ],
              )*/

              /*if (!kIsWeb &&
                  (Theme.of(context).platform == TargetPlatform.iOS ||
                      Theme.of(context).platform == TargetPlatform.android))
                SwitchListTile(
                  value: _notificationsSubscribed,
                  title: Text(S.of(context).enableMotivationgPushRemminder),
                  onChanged: _setNotificationState,
                ),*/
              /*Builder(builder: (context)=>
                  ListTile(
                    enabled: _notificationsSubscribed,
                    leading: FaIcon(FontAwesomeIcons.history),
                    title: Text('Set reminder time'),
                    onTap: () {
                      showTimePicker(
                        context: context,
                        initialTime: TimeOfDay(hour: 17),
                        locale:const Locale('en','US'),
                        builder: (BuildContext context, Widget child) {
                          return MediaQuery(
                            data: MediaQuery.of(context)
                                .copyWith(alwaysUse24HourFormat: true),
                            child: child,
                          );
                        },
                      ).then((newTime) =>
                      _notificationTime = Time(newTime.hour, newTime.minute));
                    },
                  ))*/
            ])
          ]),
        ));
  }

  Future<void> _logOut() async {
    api.call('logout', context: context).then((data) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginPage()),
          (Route<dynamic> route) => false);
    });
  }

  void _updatePassword() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(
                S.of(context).updatePassword,
                style: Theme.of(context).textTheme.headline6,
              ),
              content: Column(
                children: <Widget>[
                  Text(S
                      .of(context)
                      .pleaseProvideANewPasswordWeRecommendChoosingAPassword),
                  TextField(
                    controller: _passwordResetController,
                    decoration: InputDecoration(
                      labelText: S.of(context).newPassword,
                    ),
                    obscureText: true,
                    autofocus: true,
                  ),
                ],
              ),
              actions: <Widget>[
                MaterialButton(
                  child: Text(S.of(context).cancel),
                  onPressed: () => Navigator.pop(context),
                ),
                MaterialButton(
                  child: Text(S.of(context).updatePassword),
                  onPressed: () async {
                    api
                        .call('updateUser',
                            options: {'pass': _passwordResetController.text},
                            context: context)
                        .then((data) {
                      Navigator.pop(context);
                    });
                  },
                )
              ],
            ));
  }

/*
  void _setNotificationState(bool isEnabled) {
    if (isEnabled) {
      TestAppPush.updateSchedule(context: context, time: _notificationTime);
      Preferences().save('pushDisabled', 'false');
    } else {
      TestAppPush.unsubscribe();
      Preferences().save('pushDisabled', 'true');
    }
    setState(() {
      _notificationsSubscribed = isEnabled;
    });
  }*/
}
